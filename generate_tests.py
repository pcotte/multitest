import os
import yaml

# Define the directory containing test suites
test_dir = 'tests'

# Initialize the YAML structure with stages
pipeline = {
    'stages': ['test']
}

# Discover directories within the test directory
for test_suite in os.listdir(test_dir):
    suite_path = os.path.join(test_dir, test_suite)
    if os.path.isdir(suite_path):
        job_name = f'test_{test_suite}'
        pipeline[job_name] = {
            'stage': 'test',
            'image': 'python:3.9',
            'script': [
                'pip install pytest',
                f'echo "Running tests in {test_suite}"',
                f'pytest {suite_path}'
            ],
            # 'tags': ['your-runner-tag']
        }

# Write the generated pipeline to a YAML file
with open('generated-tests.yml', 'w') as file:
    yaml.dump(pipeline, file, default_flow_style=False)
